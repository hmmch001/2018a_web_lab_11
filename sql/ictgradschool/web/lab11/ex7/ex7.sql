DROP TABLE IF EXISTS players;
DROP TABLE IF EXISTS teams;

CREATE TABLE teams(
  name VARCHAR(50),
  id INT NOT NULL,
  home_city VARCHAR(30),
  capitan VARCHAR(50),
  total_points INT,
  PRIMARY KEY (name, id)
);

CREATE TABLE players(
  id INT NOT NULL AUTO_INCREMENT,
  fname VARCHAR(30),
  lname VARCHAR(30),
  age INT,
  nationality CHAR(2),
  team_name VARCHAR(50),
  PRIMARY KEY (id),
  FOREIGN KEY (team_name) REFERENCES teams(name)
);

INSERT INTO teams VALUES
  ('Lions', '1', 'Auckland', 'David', '350'),
  ('Tigers', '2', 'Wellington', 'Geoff', '250'),
  ('Bears', '3', 'Hamilton', 'Mark', '306'),
  ('Warriors', '4', 'Tauranga', 'Annika', '267'),
  ('Blues', '5', 'Christchurch', 'Barney', '401'),
  ('Wolves', '6', 'Dunedin', 'SpongeBob', '358'),
  ('Dragons', '7', 'Nelson', 'James', '288'),
  ('Jaguares', '8', 'Whangarei', 'Henry', '394'),
  ('Eagles', '9', 'Auckland', 'Johnnie', '321'),
  ('Hurricanes', '10', 'Wellington', 'Tia', '299'),
  ('Chiefs', '11', 'New Plymoth', 'Dom', '369'),
  ('Reds', '12', 'Palmerston North', 'Scarlett', '389'),
  ('Highlanders', '13', 'Dunedin', 'Rob', '258');


INSERT INTO players  ( fname, lname, age, nationality, team_name) VALUES
    ('David', 'Bainbridge', '24', 'AU', 'Lions'),
    ('Geoff', 'Holmes', '22', 'NZ','Tigers'),
    ('Annika', 'Hinze', '26', 'FR','Warriors'),
    ('Te Taka', 'Keegan', '27', 'MX','Chiefs'),
    ('Barney', 'Rubble', '34', 'NZ', 'Blues'),
    ('SpongeBob', 'Squarepants', '23', 'AU', 'Wolves'),
    ('James', 'Beam', '22', 'US', 'Dragons'),
    ('Jack', 'Daniels', '24', 'US', 'Reds'),
    ('James', 'Speight', '27', 'NZ', 'Highlanders'),
    ('Henry', 'Wagstaff', '28', 'NZ', 'Jaguares'),
    ('Johnnie', 'Walker', '29', 'AU', 'Eagles'),
    ('Tia', 'Maria', '30', 'MX', 'Hurricanes'),
    ('Dom', 'Perignon', '26', 'FR', 'Chiefs'),
    ('Scarlett', 'Ohara', '25', 'SC', 'Reds'),
    ('Rob', 'Roy', '27', 'SC', 'Highlanders');