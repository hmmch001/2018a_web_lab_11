-- Answers to exercise 2 questions
-- Exercise 2A
select s.fname, s.lname
from unidb_students s, unidb_attend a
where a.dept = 'comp'
      and a.num = 219
      and a.id = s.id;
-- Exercise 2B
select s.fname, s.lname
from unidb_students s, unidb_courses c
where s.country != 'NZ'
      and c.rep_id = s.id;
-- Exercise 2C
select l.office
from unidb_lecturers l, unidb_teach t
where t.num = 219
      and t.staff_no = l.staff_no;
-- Exercise 2D
select distinct s.fname, s.lname
from unidb_students s, unidb_lecturers l, unidb_teach t, unidb_attend a
where l.fname = 'Te Taka'
      and l.staff_no = t.staff_no
      and t.dept = a.dept
      and t.num = a.num
      and s.id = a.id;
-- Exercise 2E
select x.fname, x.lname, s.fname as 'Mentor fname', s.lname as 'Mentor lname'
from unidb_students s, unidb_students x
where x.mentor = s.id;


-- Exercise 2F
SELECT fname, lname
from unidb_lecturers
WHERE office LIKE 'G%'
UNION
select fname, lname
from unidb_students
where country != 'NZ';
-- Exercise 2G
select s.fname as StudentFirstName, s.lname, l.fname, l.lname
from unidb_students s, unidb_courses c, unidb_lecturers l
where c.dept = 'comp'
      and c.num = 219
      and s.id = c.rep_id
      and c.coord_no = l.staff_no;
