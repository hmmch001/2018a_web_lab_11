-- Answers to exercise 1 questions
-- Exercise 1A
SELECT DISTINCT dept from unidb_courses;
-- Exercise 1B
SELECT DISTINCT semester from unidb_attend;
-- Exercise 1C
SELECT DISTINCT dept, num from unidb_attend;
-- Exercise 1D
SELECT fname, lname, country
from unidb_students
ORDER BY fname;
-- Exercise 1E
SELECT fname, lname, mentor
from unidb_students
ORDER BY mentor;
-- Exercise 1F
SELECT *
from unidb_lecturers
ORDER BY office;
-- Exercise 1G
SELECT *
from unidb_lecturers
WHERE staff_no > 500;
-- Exercise 1H
SELECT *
from unidb_students
WHERE id >1668 AND id <1824;
-- Exercise 1I
SELECT *
from unidb_students
WHERE country = 'NZ' OR country = 'AU' OR country = 'US';
-- Exercise 1J
SELECT *
from unidb_lecturers
WHERE office LIKE 'G%';
-- Exercise 1K
SELECT *
from unidb_courses
WHERE dept != 'comp';
-- Exercise 1L
SELECT *
from unidb_students
WHERE country = 'FR' OR country = 'MX';