DROP TABLE IF EXISTS films;
DROP TABLE IF EXISTS genres;
DROP TABLE IF EXISTS actors;
DROP TABLE IF EXISTS producers;
DROP TABLE IF EXISTS certificates;
DROP TABLE IF EXISTS is_in;
DROP TABLE IF EXISTS includes;
DROP TABLE IF EXISTS belongs_to;
DROP TABLE IF EXISTS produces;

CREATE TABLE films(
  title VARCHAR(30) UNIQUE,
  PRIMARY KEY (title)
);
CREATE TABLE genres(
  genre VARCHAR(30) UNIQUE,
  PRIMARY KEY (genre)
);
CREATE TABLE actors(
  name VARCHAR(30) UNIQUE,
  PRIMARY KEY (name)
);
CREATE TABLE producers(
  name VARCHAR(30) UNIQUE,
  PRIMARY KEY (name)
);
CREATE TABLE certificates(
  name VARCHAR(30) UNIQUE,
  PRIMARY KEY (name)
);
CREATE TABLE is_in(
  name VARCHAR(30),
  title VARCHAR(30),
  role VARCHAR(30),
  PRIMARY KEY (name, title, role),
  FOREIGN KEY (name) REFERENCES actors (name),
  FOREIGN KEY (title) REFERENCES films (title)
);

CREATE TABLE includes(
  name VARCHAR(30),
  title VARCHAR(30),
  PRIMARY KEY (name, title),
  FOREIGN KEY (name) REFERENCES certificates (name),
  FOREIGN KEY (title) REFERENCES films (title)
);
CREATE TABLE belongs_to(
  genre VARCHAR(30),
  title VARCHAR(30),
  PRIMARY KEY (genre, title),
  FOREIGN KEY (genre) REFERENCES genres (genre),
  FOREIGN KEY (title) REFERENCES films (title)
);
CREATE TABLE produces(
  name VARCHAR(30),
  title VARCHAR(30),
  PRIMARY KEY (name, title),
  FOREIGN KEY (name) REFERENCES producers (name),
  FOREIGN KEY (title) REFERENCES films (title)
);